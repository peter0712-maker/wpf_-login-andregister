﻿using System.Collections;

namespace System.Linq
{
    internal class SystemCore_EnumerableDebugView
    {
        private ICollection keys;

        public SystemCore_EnumerableDebugView(ICollection keys)
        {
            this.keys = keys;
        }

        public object Items { get; internal set; }
    }
}