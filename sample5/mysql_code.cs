﻿using System;
using System.Collections;
using System.Data;
using System.Windows;
//引入MySQL
using MySql.Data.MySqlClient;

namespace sample5
{
    public class Mysql_Code
    {
        static string constring = "server = 'localhost'; uid = 'root'; pwd = 'djz19960824'; database = 'test';";   //定义连接mysql字符串
        MySqlConnection sqlCnn = new MySqlConnection(constring);    //连接mysql
        public Hashtable Mysql_read()
        {
            string username;
            string password;
            string cmdstring = "select * from code";    //写入sql
            MySqlCommand sqlCmd = new MySqlCommand(cmdstring, sqlCnn);   //创建命令对象  
            Hashtable h = new Hashtable();

            try
            {
                sqlCnn.Open();  //打开数据库
                MySqlDataReader rec = sqlCmd.ExecuteReader();

                while (rec.Read())
                {
                    username = rec.GetString(0);
                    password = rec.GetString(1);
                    h.Add(username, password);
                }
                return h;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "error");
                return null;
            }
            finally
            {
                sqlCnn.Close();
            }
        }

        public void Mysql_insert(string u, string p)
        {
            try
            {
                sqlCnn.Close();
                sqlCnn.Dispose();
                string insertstring = "insert into code values('" + u + "','" + p + "')";
                MySqlCommand sqlInsert = new MySqlCommand(insertstring, sqlCnn);
                sqlCnn.Open();
                sqlInsert.ExecuteNonQuery();    //插入数据
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "error");
            }
            finally
            {
                sqlCnn.Close();
            }

        }
    }
}
