﻿using System;
using System.Collections;
using System.Windows;
using System.Windows.Input;
using Window = System.Windows.Window;


namespace sample5
{
    /// <summary>
    /// RegisterWindow.xaml 的交互逻辑
    /// </summary>
    public partial class RegisterWindow : Window
    {

        public static Hashtable userall;
        public RegisterWindow()
        {
            InitializeComponent();
        }
        private void MoveWindow_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            DragMove();
        }

        private void ReClose_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void Register_Button(object sender, RoutedEventArgs e)
        {
            string u = Re_Account.Text.ToString();
            string p = Re_Password.Password.ToString();
            string rp = Re_PasswordAgain.Password.ToString();
            //ExcelSave excel = new ExcelSave();
            Mysql_Code mysql = new Mysql_Code();


            if (String.IsNullOrEmpty(u))
            {
                MessageBox.Show("user is not null");
                return;
            }
            if (String.IsNullOrEmpty(p))
            {
                MessageBox.Show("password is not null");
                return;
            }
            if (String.IsNullOrEmpty(rp))
            {
                MessageBox.Show("Repassword is not null");
                return;
            }
            if (!p.Equals(rp))
            {
                MessageBox.Show("password is not equals repassword");
                return;
            }

            userall = mysql.Mysql_read();
            //userall = excel.readExcel();  //读取excel数据
            if (userall == null)
            {
                userall = new Hashtable();
                userall.Add(u, p);
            }
            else
            {
                bool isexist = userall.ContainsKey(u);  //判断用户是否存在
                if (isexist)
                {
                    MessageBox.Show("user is exist!");
                    return;
                }
                else
                {
                    userall.Add(u, p);
                    Console.WriteLine(userall.Count);
                }
            }

            System.Windows.Application.Current.Properties["users"] = userall;   //类似于Session的功能,用户登录后，可以将用户的信息保存在Properties中。
            //excel.InsertExcel(u, p);
            mysql.Mysql_insert(u, p);
            MessageBox.Show("regist success!");


            MainWindow main = new MainWindow();
            main.WindowStartupLocation = WindowStartupLocation.Manual;   //使新窗口位置在原来的位置上
            main.Left = this.Left;  //使新窗口位置在原来的位置上
            main.Top = this.Top;  //使新窗口位置在原来的位置上
            this.Close();
            main.ShowDialog();  //打开新窗口         
        }
    }
}
