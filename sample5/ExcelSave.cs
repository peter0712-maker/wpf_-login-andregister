﻿using System;
using Excel = Microsoft.Office.Interop.Excel;
using System.Collections;
using System.Windows;

namespace sample5
{

    public class ExcelSave
    {
        Excel.Application ExcelApp = new Excel.Application();
        #region 创建excel工作簿
        public void InsertExcel(string u, string p)
        {
            //1.创建Excel

            try
            {
                //2.打开已经存在的工作簿
                string path = "C:\\Users\\非黑不即白\\Desktop\\code.xlsx";
                ExcelApp.Workbooks.Open(path, ReadOnly: false);

                //3.
                ExcelApp.Cells[1, 1].Value = "username";
                ExcelApp.Cells[1, 2].Value = "password";

                int RowCount = ExcelApp.ActiveSheet.UsedRange.Rows.Count + 1;
                //ExcelApp.ActiveSheet.Rows[RowCount].Insert(u, p);

                ExcelApp.Cells[RowCount, 1].Value = u;
                ExcelApp.Cells[RowCount, 2].Value = p;


                ExcelApp.DisplayAlerts = false; //保存Excel的时候，不弹出是否保存的窗口直接进行保存 
                //4.保存工作表
                ExcelApp.ActiveWorkbook.Save();

            }
            catch
            {
                MessageBox.Show("导出文件保存失败,可能原因该文件已打开！", "警告！");
            }
            finally
            {
                //5.关闭工作簿
                ExcelApp.ActiveWorkbook.Close();
                //6.退出excel
                ExcelApp.Quit();
                // PublicMethod.Kill(ExcelApp);
            }

        }
        #endregion

        #region 读取excel工作簿数据
        public Hashtable readExcel()
        {
            try
            {
                //2.打开已经存在的工作簿
                string path = "C:\\Users\\非黑不即白\\Desktop\\code.xlsx";
                //ExcelApp.Workbooks.Open(path, ReadOnly: true);
                Hashtable h = new Hashtable();


                Excel.Workbook wb = ExcelApp.Application.Workbooks.Open(path, ReadOnly: true); //取得工作簿
                Excel.Worksheet ws = (Excel.Worksheet)wb.Worksheets.get_Item(1);

                int rowsint = ws.UsedRange.Cells.Rows.Count; //得到行数

                Excel.Range rng1 = ws.Cells.get_Range("A2", "A" + rowsint); // 取得数据范围区域(不包括标题列)
                Excel.Range rng2 = ws.Cells.get_Range("B2", "B" + rowsint);

                string[,] arry = new string[rowsint - 1, 2];  //将新值赋给一个数组

                if (rowsint <= 1)
                {
                    //MessageBox.Show("无账户信息，请先注册!");  LoginWindow已做判断，无需重复
                    return null;
                }
                else
                {
                    if (rowsint == 2)  //解决表格有一个数据用户，再添加时报错的问题
                    {
                        arry[0, 0] = rng1.Value2.ToString();   //rng.value2获取单元格数据
                        arry[0, 1] = rng2.Value2.ToString();
                        h.Add(rng1.Value2.ToString(), rng2.Value2.ToString());
                        Console.WriteLine(rng1.Value2.ToString() + " :" + rng2.Value2.ToString());
                        return h;
                    }
                    else
                    {
                        for (int i = 1; i <= rowsint - 1; i++)
                        {
                            arry[i - 1, 0] = rng1.Value2[i, 1].ToString();   //rng.value2获取单元格数据
                            arry[i - 1, 1] = rng2.Value2[i, 1].ToString();
                            h.Add(rng1.Value2[i, 1].ToString(), rng2.Value2[i, 1].ToString());
                            Console.WriteLine(rng1.Value2[i, 1].ToString() + " :" + rng2.Value2[i, 1].ToString());
                        }
                        return h;
                    }
                }
            }
            catch
            {
                MessageBox.Show("read excel error");
                return null;
            }
            finally
            {
                //5.关闭工作簿
                ExcelApp.ActiveWorkbook.Close();
                //6.退出excel
                ExcelApp.Quit();
                //PublicMethod.Kill(ExcelApp); //关闭excel后台进程，此处无需添加
            }
        }
        #endregion
    }

    /* 关闭excel后台进程
    public class PublicMethod
    {
        [DllImport("User32.dll", CharSet = CharSet.Auto)]
        public static extern int GetWindowThreadProcessId(IntPtr hwnd, out int ID);
        public static void Kill(Excel.Application excel)
        {
            IntPtr t = new IntPtr(excel.Hwnd);//得到这个句柄，具体作用是得到这块内存入口 

            int k = 0;
            GetWindowThreadProcessId(t, out k);   //得到本进程唯一标志k
            System.Diagnostics.Process p = System.Diagnostics.Process.GetProcessById(k);   //得到对进程k的引用
            p.Kill();     //关闭进程k
        }
    }*/
}
